#region Namespaces

using System;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using GirlKillsDragon.Plots;
using Xunit;
using Xunit.Abstractions;

#endregion

namespace GirlKillsDragon.Tests
{
	public class PlotDateTimeResolverTests : PlotTestBase
	{
		private readonly DateTime _start;

		private readonly PlotDateTime _startTime;

		public PlotDateTimeResolverTests(ITestOutputHelper output)
			: base(output)
		{
			_start = new DateTime(2001, 2, 3);
			_startTime = new PlotDateTime(_start);
		}

		/// <summary>
		/// If we don't even have an initial time to work with, we can't resolve anything and should
		/// blow up.
		/// </summary>
		[Fact]
		public void CannotResolvePlotWithoutSpecificTime()
		{
			Nodes.Add(new Node());

			var exception = Assert.Throws<InvalidOperationException>(() => PlotDateTimeResolver.Resolve());

			Assert.Equal("Cannot find a matching node.", exception.Message);
		}

		[Fact]
		public void CanResolveThroughSingleLink()
		{
			var plot1 = Nodes.InitialNode = new Node {DateTime = _startTime};
			var plot2 = new Node();

			Nodes.Add(plot1, plot2);
			Graph.Connect(plot1, plot2);

			PlotDateTimeResolver.Resolve();

			Assert.Equal(_start, plot1.DateTime.Resolved);
			Assert.Equal(_start.AddMinutes(1), plot2.DateTime.Resolved);
		}

		/// <summary>
		/// If we have a specific time, then we can successfully resolve all the times.
		/// </summary>
		[Fact]
		public void ResolveSinglePlotWithSpecificTime()
		{
			var plot = Nodes.InitialNode = new Node
			{
				DateTime = _startTime
			};

			Nodes.Add(plot);
			PlotDateTimeResolver.Resolve();

			Assert.Equal(_start, Nodes[0].DateTime.Resolved);
		}

		/// <summary>
		/// With fights, we have a death and end of fight pairs.
		/// </summary>
		[Fact]
		public void ResolveTwoRelatedPlotsInTheMiddle()
		{
			// Create the nodes for this fight.
			var location = new Location(-1);
			var leave = new LeaveNode(location);
			var begin = Nodes.InitialNode = new BeginFightNode(location)
			{
				DateTime = _startTime
			};
			var death = new Node(location);
			var end = new EndFightNode(location).AddAfterPlot(death).AddBeforePlot(leave);

			Nodes.Add(begin, death, end, leave);
			Graph.Connect(begin, death);
			Graph.Connect(begin, end);
			Graph.Connect(end, leave);

			// Resolve all the date times.
			PlotDateTimeResolver.Resolve();

			// Verify the dates of the items.
			Assert.False(death.DateTime.Specific.HasValue);
			Assert.False(end.DateTime.Specific.HasValue);
			Assert.False(leave.DateTime.Specific.HasValue);

			Assert.Equal(_start.AddMinutes(0), begin.DateTime.Resolved);
			Assert.Equal(_start.AddMinutes(1), death.DateTime.Resolved);
			Assert.Equal(_start.AddMinutes(2), end.DateTime.Resolved);
			Assert.Equal(_start.AddMinutes(3), leave.DateTime.Resolved);
		}
	}
}
