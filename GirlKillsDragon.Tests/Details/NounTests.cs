#region Namespaces

using GirlKillsDragon.Details;
using Xunit;
using Xunit.Abstractions;

#endregion

namespace GirlKillsDragon.Tests.Details
{
	public class NounTests : PlotTestBase
	{
		public NounTests(ITestOutputHelper output) : base(output)
		{
		}

		[Fact]
		public void CreateHuman()
		{
			// The noun template collections basically defines the "slots" that can have
			// nouns and can be described. This is what the sentence builders work on.
			var chaos = new Chaos();
			var templates = new NounTemplateManager(chaos)
			{
				new NounTemplate("human", "hair", "gender-expression", "face"),
				new NounTemplate("hair"),
				new NounTemplate("face", "face-shape", "eye-color"),
				new NounTemplate("face-shape"),
				new NounTemplate("eye-color"),
				new NounTemplate("gender-expression")
			};

			// This is what the code looks for when it needs to generate an actor or an environment.
			// It uses a selector (the first field) to choose it and then creates a template of nouns
			// based on the base template ID. It also allows for specific settings, such as gender or
			// age.
			var selectors = new NounSelectorManager(chaos)
			{
				new NounSelector("human", "actor human")
				{
					// We're forcing additional filters on the gender expression.
					new NounSelectorDetail("gender-expression", "female-gender-expression")
				}
			};

			// We need to define details for these components.
			var details = new DetailSetManager(chaos)
			{
				new DetailSet("gender-expression male-gender-expression")
				{
					new Detail(DetailType.Is, "a boy")
				},
				new DetailSet("gender-expression female-gender-expression")
				{
					new Detail(DetailType.Is, "a girl")
				},
				new DetailSet("hair")
				{
					new Detail(DetailType.Is, "a red-headed"),
					new Detail(DetailType.Has, "red hair")
				},
				new DetailSet("face-shape")
				{
					new Detail(DetailType.Has, "a heart-shaped face")
				},
				new DetailSet("eye-color")
				{
					new Detail(DetailType.Has, "green eyes")
				}
			};

			// We need to create a noun (person).
			var nounFactory = new NounFactory(Log, templates, selectors, details);
			var noun = nounFactory.Create("human");

			// Verify a straight lookup.
			Assert.Equal("a girl", noun.GetDetails("gender-expression")[0].Details[0].Value);

			// Verify a random lookup of the ":of" selector.
			Assert.Equal("a heart-shaped face", noun.GetDetails("face:of")[0].Details[0].Value);
			Assert.Equal("green eyes", noun.GetDetails("face:of")[1].Details[0].Value);

			// Look up details based on has.
			Assert.Equal("red hair", noun.GetDetails("hair:of :has")[0].HasDetails[0].Value);
			Assert.Equal("a red-headed", noun.GetDetails("hair:of :is")[0].IsDetails[0].Value);
		}

		[Fact]
		public void InjectDetail()
		{
			// Set up the details.
			var noun = new Noun();
			var detail = new Detail(DetailType.Is, "a redhead");

			noun.AddDetails("hair", detail);

			// Look up the detail.
			Assert.Equal("a redhead", noun.GetDetails("hair")[0][0].Value);
		}

		[Fact]
		public void InjectDetailSet()
		{
			// Set up the details.
			var noun = new Noun();
			var details = new DetailSet
			{
				new Detail(DetailType.Is, "a redhead")
			};

			noun.AddDetails("hair", details);

			// Look up the detail.
			Assert.Equal("a redhead", noun.GetDetails("hair")[0][0].Value);
		}
	}
}
