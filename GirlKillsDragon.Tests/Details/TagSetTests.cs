#region Namespaces

using GirlKillsDragon.Details;
using Xunit;

#endregion

namespace GirlKillsDragon.Tests.Details
{
	public class TagSetTests
	{
		[Fact]
		public void DuplicateTags()
		{
			var tags = new TagSet("Tag1 Tag1");

			Assert.Equal("tag1", tags.ToString());
		}

		[Fact]
		public void EmptyMatchesNothing()
		{
			var tags = new TagSet();
			var test = new TagSet("tag1");

			Assert.False(tags.IsMatch(test));
		}

		[Fact]
		public void EmptyTagSetToString()
		{
			var tags = new TagSet();

			Assert.Equal("", tags.ToString());
		}

		[Fact]
		public void MatchIdentical()
		{
			var tags = new TagSet("tag1");
			var test = new TagSet("tag1");

			Assert.True(tags.IsMatch(test));
		}

		[Fact]
		public void MatchNone()
		{
			var tags = new TagSet("tag1");
			var test = new TagSet("tag2");

			Assert.False(tags.IsMatch(test));
		}

		[Fact]
		public void MatchSubset()
		{
			var tags = new TagSet("tag1 tag2");
			var test = new TagSet("tag1");

			Assert.False(tags.IsMatch(test));
		}

		[Fact]
		public void MatchSuperset()
		{
			var tags = new TagSet("tag1");
			var test = new TagSet("tag1 tag2");

			Assert.True(tags.IsMatch(test));
		}

		[Fact]
		public void MultipleItemTagSet()
		{
			var tags = new TagSet("Tag1 Tag2");

			Assert.Equal("tag1 tag2", tags.ToString());
		}

		[Fact]
		public void SingleItemTagSetWithCapital()
		{
			var tags = new TagSet("Tag");

			Assert.Equal("tag", tags.ToString());
		}
	}
}
