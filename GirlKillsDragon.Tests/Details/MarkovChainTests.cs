#region Namespaces

using GirlKillsDragon.Details;
using Xunit;

#endregion

namespace GirlKillsDragon.Tests.Details
{
	public class MarkovChainTests
	{
		[Fact]
		public void CreateFromMultipleNames()
		{
			var chain = new MarkovChain(new Chaos(), 4)
			{
				Titleize = false
			};

			chain.Add("dylan");
			chain.Add("dyfan");
			chain.Add("dymas");

			Assert.Equal("dymas", chain.Create());
			Assert.Equal("dylan", chain.Create());
			Assert.Equal("dyfan", chain.Create());
			Assert.Equal("dymas", chain.Create());
			Assert.Equal("dymas", chain.Create());
			Assert.Equal("dymas", chain.Create());
			Assert.Equal("dymas", chain.Create());
		}

		[Fact]
		public void CreateLongName()
		{
			var chain = new MarkovChain(new Chaos(), 4)
			{
				Titleize = false
			};

			chain.Add("moonfire");

			var results = chain.Create();

			Assert.Equal("moonfire", results);
		}

		[Fact]
		public void ParseLongName()
		{
			var chain = new MarkovChain(new Chaos(), 4)
			{
				Titleize = false
			};

			chain.Add("moonfire");
		}
	}
}
