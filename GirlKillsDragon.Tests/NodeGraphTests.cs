#region Namespaces

using System;
using GirlKillsDragon.Nodes;
using GirlKillsDragon.Plots;
using Xunit;
using Xunit.Abstractions;

#endregion

namespace GirlKillsDragon.Tests
{
	/// <summary>
	/// Various tests for testing the basic functionality of the PlotGraph class.
	/// </summary>
	public class NodeGraphTests : PlotTestBase
	{
		public NodeGraphTests(ITestOutputHelper output) : base(output)
		{
		}

		[Fact]
		public void GetPresentActors()
		{
			// We want to turn off name resolution.
			CountryActorNamingResolver.Enabled = false;

			// We have two actors we care about.
			var actor1 = Actors.Create();
			var actor2 = Actors.Create();

			// We also need to have events in one location before moving to the other.
			var location1 = Locations.Create();
			var location2 = Locations.Create();

			// Actors are born to enter them into the system.
			var born1 = new BornNode(location1, actor1);
			var born2 = new BornNode(location1, actor2);

			// They have to "arrive".
			var arrive1 = new ArriveNode(location1);
			var arrive2 = new ArriveNode(location1);

			// They have a fight.
			var fight = new BeginFightNode(location1);

			// The second leaves for a new location.
			var leave3 = new LeaveNode(location1);
			var arrive3 = new ArriveNode(location2);

			// The first actor dies.
			var death1 = new DeathNode(location1, actor1).AddBeforePlot(leave3);
			var death2 = new DeathNode(location1, actor2);

			// Add all the nodes together.
			var locationPlots1 = new[] {born1, born2, arrive1, arrive2, fight, death1, death2, leave3};
			var locationPlots2 = new Node[] {arrive3};

			Nodes.InitialNode = born1;
			Nodes.InitialNode.DateTime = new PlotDateTime(new DateTime(2001, 1, 1));

			// Connect all the node together.
			Graph.Connect(born1, arrive1, actor1);
			Graph.Connect(born2, arrive2, actor2);
			Graph.Connect(arrive1, fight, actor1);
			Graph.Connect(arrive2, fight, actor2);
			Graph.Connect(fight, death1, actor1);
			Graph.Connect(fight, leave3, actor2);
			Graph.Connect(leave3, arrive3, actor2);
			Graph.Connect(arrive3, death2, actor2);

			// We have to resolve all the date times to get this working.
			ValidateAndResolve();

			// And the crux of the people. Who is present at these last few nodes?
			Assert.Equal(new string[] { }, Graph.GetPresentActors(fight, locationPlots1).Names);
			Assert.Equal(new string[] { }, Graph.GetPresentActors(death1, locationPlots1).Names);
			Assert.Equal(new string[] { }, Graph.GetPresentActors(leave3, locationPlots1).Names);
			Assert.Equal(new string[] { }, Graph.GetPresentActors(arrive3, locationPlots2).Names);

			Assert.Equal(new[] {"A0", "A1"}, Graph.GetPresentActors(fight).Names);
			Assert.Equal(new[] {"A0", "A1"}, Graph.GetPresentActors(death1).Names);
			Assert.Equal(new[] {"A1"}, Graph.GetPresentActors(leave3).Names);
			Assert.Equal(new string[] { }, Graph.GetPresentActors(arrive3).Names);
		}

		[Fact]
		public void RemoveNodeInChain()
		{
			// We need a location.
			var location1 = Locations.Create();

			// Create a chain of three nodes.
			var node1 = new Node(location1);
			var node2 = new Node(location1);
			var node3 = new Node(location1);

			Nodes.Add(node1, node2, node3);
			Graph.Connect(node1, node2);
			Graph.Connect(node2, node3);

			// Now remove the middle one.
			Graph.Remove(node2);

			// Verify the results.
			Assert.Equal(new[] {node1}, Nodes);
			Assert.Equal(new Edge[] { }, node1.OutEdges);
		}

		[Fact]
		public void RemoveNodeInFork()
		{
			// We need a location.
			var location1 = Locations.Create();

			// Create a chain of three nodes.
			var node1 = new Node(location1);
			var node2 = new Node(location1);
			var node3 = new Node(location1);
			var node4 = new Node(location1);

			Nodes.Add(node1, node2, node3, node4);
			Graph.Connect(node1, node2);
			Graph.Connect(node1, node3);
			Graph.Connect(node2, node4);
			Graph.Connect(node3, node4);

			// Now remove the middle one.
			Graph.Remove(node2);

			// Verify the results.
			Assert.Equal(new[] {node1, node3, node4}, Nodes);
			Assert.Equal(new[] {node3}, node1.OutNodes);
			Assert.Equal(new[] {node4}, node3.OutNodes);
			Assert.Equal(new[] {node1}, node3.InNodes);
			Assert.Equal(new[] {node3}, node4.InNodes);
		}
	}
}
