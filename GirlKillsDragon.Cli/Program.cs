#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using CommandLine;
using GirlKillsDragon.Cli.Commands;
using Semver;
using Serilog;
using Serilog.Core;
using ParserResultExtensions = CommandLine.ParserResultExtensions;

#endregion

namespace GirlKillsDragon.Cli
{
	public class Program
	{
		public static void Main(string[] args)
		{
			// Set up logging and show that it is working.
			var levelSwitch = new LoggingLevelSwitch();
			var log = new LoggerConfiguration()
				.MinimumLevel.ControlledBy(levelSwitch)
				.WriteTo.LiterateConsole()
				.WriteTo.CountingSink(out var countingSink)
				.CreateLogger();

			// Figure out the version of our library.
			var cliVersion =
				new SemVersion(typeof(Program).Assembly.GetName().Version);
			var coreVersion =
				new SemVersion(typeof(CoreModule).Assembly.GetName().Version);

			log.Information(
				"Girl Kills Dragon v{0} (CLI v{1})",
				coreVersion,
				cliVersion);

			// Set up the DI/IoC container which wires everything together.
			var builder = new ContainerBuilder();

			builder.RegisterInstance(log).As<ILogger>().SingleInstance();
			builder.RegisterInstance(levelSwitch).As<LoggingLevelSwitch>();
			builder.RegisterInstance(new FileSystem()).As<FileSystem>();
			builder.RegisterAssemblyModules(typeof(CoreModule).Assembly);
			builder.RegisterAssemblyModules(typeof(CliModule).Assembly);

			var container = builder.Build();

			// Parse the arguments using the commands.
			var commands = container
				.Resolve<IEnumerable<ICommandOptions>>()
				.Select(c => c.GetType())
				.ToList();
			var parser = new Parser(with => with.EnableDashDash = true);

			ParserResultExtensions.WithNotParsed(
					parser.ParseArguments(args, commands.ToArray()),
					errors =>
					{
						log.Error("Could not parse command line arguments");

						foreach (var error in errors)
						{
							log.Error("  {0}", error);
						}
					})
				.WithParsed(
					parsed =>
					{
						// Perform the command based on the options.
						var commandOptions = (CommandOptionsBase) parsed;
						var commandType = commandOptions.CommandType;
						var command = (ICommand) container.Resolve(commandType);

						command.CommandOptions = commandOptions;
						command.Run();

						// Figure out if we need to pause.
						var pause = commandOptions.Pause;

						pause |= commandOptions.PauseOnError && countingSink.HasErrors;
						pause |= commandOptions.PauseOnWarning && countingSink.HasWarningsOrErrors;

						if (pause)
						{
							Console.WriteLine("Press any key to continue.");
							Console.ReadKey();
						}
					});
		}
	}
}
