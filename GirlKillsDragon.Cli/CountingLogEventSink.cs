#region Namespaces

using Serilog.Core;
using Serilog.Events;

#endregion

namespace GirlKillsDragon.Cli
{
	/// <summary>
	/// Simple log sink that keeps tracks of the errors and warnings.
	/// </summary>
	public class CountingLogEventSink : ILogEventSink
	{
		public int Errors { get; set; }

		public bool HasErrors => Errors > 0;

		public bool HasWarningsOrErrors => HasErrors || Warnings > 0;

		public int Warnings { get; set; }

		public void Emit(LogEvent logEvent)
		{
			if (logEvent.Level == LogEventLevel.Error)
			{
				Errors++;
			}

			if (logEvent.Level == LogEventLevel.Warning)
			{
				Warnings++;
			}
		}
	}
}
