#region Namespaces

using Serilog.Core;
using Serilog.Events;

#endregion

namespace GirlKillsDragon.Cli.Commands
{
	public abstract class CommandBase<TOptions> : ICommand
		where TOptions : CommandOptionsBase
	{
		public LoggingLevelSwitch LoggingLevelSwitch { get; set; }

		public TOptions Options => (TOptions) CommandOptions;

		public ICommandOptions CommandOptions { get; set; }

		public virtual void Run()
		{
			if (Options.Verbose)
			{
				LoggingLevelSwitch.MinimumLevel = LogEventLevel.Verbose;
			}
			else if (Options.Debug)
			{
				LoggingLevelSwitch.MinimumLevel = LogEventLevel.Debug;
			}
		}
	}
}
