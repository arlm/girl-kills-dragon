#region Namespaces

using System.Xml.Serialization;
using GirlKillsDragon.Writing;

#endregion

namespace GirlKillsDragon.IO
{
	public struct DagCategory
	{
		[XmlAttribute] public string Id;

		[XmlAttribute] public string Background;

		public DagCategory(Chapter chapter)
			: this()
		{
			Id = $"Chapter {chapter.Number}";
		}
	}
}
