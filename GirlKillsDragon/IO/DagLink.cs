#region Namespaces

using System.Xml.Serialization;
using GirlKillsDragon.Extensions;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using GirlKillsDragon.Writing;

#endregion

namespace GirlKillsDragon.IO
{
	[XmlRoot("Link")]
	public struct DagLink
	{
		[XmlAttribute] public string Source;

		[XmlAttribute] public string Target;

		[XmlAttribute] public string Label;

		[XmlAttribute] public string Actors;

		[XmlAttribute] public string StrokeThickness;

		[XmlAttribute] public string Stroke;

		[XmlAttribute] public string StrokeDashArray;

		[XmlAttribute] public string Background;

		[XmlAttribute] public string Foreground;

		[XmlAttribute] public string Category;

		public DagLink(string source, string target, string label)
			: this()
		{
			Source = source;
			Target = target;
			Label = label;
		}

		public DagLink(Edge edge)
			: this()
		{
			var newLocation = edge.HeadNode.Location != edge.TailNode.Location;

			Actors = edge.ActorNames.OxfordAndJoin();
			Source = $"N{edge.HeadNode.NodeId}";
			Target = $"N{edge.TailNode.NodeId}";
			Label = Actors;
			StrokeThickness = newLocation ? "4" : "1";
			Stroke = newLocation ? "#FFD4AA" : "#6B949E";
		}

		public DagLink(Route route)
			: this()
		{
			Source = $"L{route.Location1.LocationId}";
			Target = $"L{route.Location2.LocationId}";
			Label = $"{route.Meters / 1000:N2} km";
		}

		public DagLink(Node beforeNode, Node afterNode)
			: this()
		{
			Source = $"N{beforeNode.NodeId}";
			Target = $"N{afterNode.NodeId}";
			Label = $"{beforeNode}\n{afterNode}";
			StrokeDashArray = "1,1";
		}

		public DagLink(Chapter chapter, Node node)
			: this()
		{
			Source = $"Chapter {chapter.Number}";
			Target = $"N{node.NodeId}";
			Category = "Contains";
		}

		public DagLink(Chapter chapter)
			: this()
		{
			Source = $"Chapter {chapter.Number - 1}";
			Target = $"Chapter {chapter.Number}";
			Category = "Chapters";
		}
	}
}
