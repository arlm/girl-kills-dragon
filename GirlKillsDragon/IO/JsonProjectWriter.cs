#region Namespaces

using System.IO;
using GirlKillsDragon.Nodes;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

#endregion

namespace GirlKillsDragon.IO
{
	public class JsonProjectWriter
	{
		public JsonProjectWriter(NodeManager nodeManager)
		{
			NodeManager = nodeManager;
		}

		public NodeManager NodeManager { get; }

		public void Write(StreamWriter writer)
		{
			var serializer = new JsonSerializer
			{
				ContractResolver = new CamelCasePropertyNamesContractResolver(),
				Formatting = Formatting.Indented,
				NullValueHandling = NullValueHandling.Ignore
			};

			serializer.Serialize(writer, this);
		}
	}
}
