#region Namespaces

using System.Collections.Generic;

#endregion

namespace GirlKillsDragon.IO
{
	public class DetailsData
	{
		public List<DetailSelectorData> Selectors { get; set; }

		public List<DetailSetData> Sets { get; set; }

		public Dictionary<string, List<string>> Templates { get; set; }
	}
}
