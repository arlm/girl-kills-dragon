#region Namespaces

using System.IO;
using System.Xml;
using System.Xml.Serialization;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.IO
{
	/// <summary>
	/// A DGML file writer that writes out the plot graph.
	/// </summary>
	public class DgmlProjectWriter : DagProjectWriter
	{
		public DgmlProjectWriter(NodeManager nodes, LocationManager locations)
			: base(nodes, locations)
		{
		}

		protected override void Write(StreamWriter writer, DagGraph graph)
		{
			var root = new XmlRootAttribute("DirectedGraph") {Namespace = "http://schemas.microsoft.com/vs/2009/dgml"};
			var serializer = new XmlSerializer(typeof(DagGraph), root);
			var settings = new XmlWriterSettings
			{
				Indent = true,
				NewLineChars = "\n"
			};

			using (var xmlWriter = XmlWriter.Create(writer, settings))
			{
				serializer.Serialize(xmlWriter, graph);
				xmlWriter.Close();
			}
		}
	}
}
