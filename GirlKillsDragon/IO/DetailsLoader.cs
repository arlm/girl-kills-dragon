#region Namespaces

using System.IO;
using GirlKillsDragon.Details;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

#endregion

namespace GirlKillsDragon.IO
{
	public class DetailsLoader
	{
		private readonly FileSystem _fileSystem;

		private readonly NounSelectorManager _selectors;

		private readonly DetailSetManager _sets;

		private readonly NounTemplateManager _templates;

		public DetailsLoader(
			FileSystem fileSystem,
			NounTemplateManager templates,
			NounSelectorManager selectors,
			DetailSetManager sets)
		{
			_fileSystem = fileSystem;
			_templates = templates;
			_selectors = selectors;
			_sets = sets;
		}

		public void Load()
		{
			foreach (var file in _fileSystem.GetDetailFiles())
			{
				Load(file);
			}
		}

		public void Load(FileInfo file)
		{
			// Load the YAML into memory.
			var deserializer = new DeserializerBuilder()
				.WithNamingConvention(new CamelCaseNamingConvention())
				.IgnoreUnmatchedProperties()
				.Build();
			DetailsData data;

			using (var stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
			using (var reader = new StreamReader(stream))
			{
				data = deserializer.Deserialize<DetailsData>(reader);
			}

			// Add in the various components.
			LoadTemplates(data);
			LoadSelectors(data);
			LoadSets(data);
		}

		private void LoadSelectors(DetailsData data)
		{
			// Ignore blanks.
			if (data.Selectors == null)
			{
				return;
			}

			// Add in the selectors.
			foreach (var selectorData in data.Selectors)
			{
				var selector = new NounSelector(
					selectorData.Selector,
					selectorData.Tags);

				foreach (var detailData in selectorData.Details)
				{
					var detail = new NounSelectorDetail(
						detailData.Key,
						detailData.Value);

					selector.Add(detail);
				}

				_selectors.Add(selector);
			}
		}

		private void LoadSets(DetailsData data)
		{
			if (data.Sets == null)
			{
				return;
			}

			foreach (var setData in data.Sets)
			{
				var set = new DetailSet(setData.Tags);

				foreach (var detailData in setData.Details)
				{
					var detail = new Detail(detailData.Type, detailData.Value);

					set.Add(detail);
				}

				_sets.Add(set);
			}
		}

		private void LoadTemplates(DetailsData data)
		{
			if (data.Templates == null)
			{
				return;
			}

			foreach (var pair in data.Templates)
			{
				var template = new NounTemplate(pair.Key, pair.Value.ToArray());

				_templates.Add(template);
			}
		}
	}
}
