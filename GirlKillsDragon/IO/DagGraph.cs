#region Namespaces

using System.Xml.Serialization;

#endregion

namespace GirlKillsDragon.IO
{
	[XmlRoot("Graph")]
	public struct DagGraph
	{
		[XmlArrayItem("Node")] public DagNode[] Nodes;

		[XmlArrayItem("Link")] public DagLink[] Links;

		[XmlArrayItem("Category")] public DagCategory[] Categories;
	}
}
