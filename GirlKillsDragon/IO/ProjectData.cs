#region Namespaces

using System;

#endregion

namespace GirlKillsDragon.IO
{
	public class ProjectData
	{
		/// <summary>
		/// Contains the starting date time.
		/// </summary>
		public DateTime? DateTime { get; set; }

		public int Narrative { get; set; }

		/// <summary>
		/// Gets or sets the numeric seed used by the project.
		/// </summary>
		public int Seed { get; set; }
	}
}
