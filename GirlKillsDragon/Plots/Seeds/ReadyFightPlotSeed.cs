#region Namespaces

using System;
using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Seeds
{
	/// <summary>
	/// Creates a basic plot seed for a fight between two characters.
	/// </summary>
	public class ReadyFightPlotSeed : PlotSeedBase
	{
		public override void Seed()
		{
			// The basic plot involves just two characters, a hero and a villain.
			var hero = Actors.Create().SetSignificant();
			var villain = Actors.Create().SetSignificant();

			Log.Warning("We don't have the ability to set the villian's age yet");
			villain.Age = TimeSpan.FromDays(365 * 214);

			Log.Information("Ready Fight: {0} v. {1}", hero, villain);

			// Naturally, they have places where they are located.
			var heroLocation = Locations.Create();
			var villianBornLocation = Locations.Create();
			var villainLocation = Locations.Create();

			// The villian needs to be introduced before we start the story. We
			// don't show this section but we do this so everything follows the
			// same basic logic. We mark this as a prologue because prologues
			// are evil and should never be shown.
			var villianBorn = new BornNode(villianBornLocation, villain)
			{
				NodeType = NodeType.Past
			};
			var villianLeave = new LeaveNode(villianBornLocation)
			{
				NodeType = NodeType.Past
			};
			var villianArrive = new ArriveNode(villainLocation)
			{
				NodeType = NodeType.Past
			};

			// The hero probably should also be born or something. This is a
			// prologue because no one cares when or how they were born.
			var introduceHero = new BornNode(heroLocation, hero)
			{
				NodeType = NodeType.Past
			};

			// We need the point the novel start. This is also marked as
			// the starting point of the novel by having the time specifically
			// set. This will be the only one that starts with a specific time
			// so the rest of the plot timing will be resolved from this one.
			var inciting = new IncitingNode(heroLocation)
			{
				DateTime = new PlotDateTime(Project.Start)
			};

			// Since the hero isn't at the same location. The murder hobo needs
			// to visit the villain to kill them. Also, it should take some time
			// to travel so we are going to have a random number of days.
			var heroLeave = new LeaveNode(heroLocation);
			var heroArrive = new ArriveNode(villainLocation).AddAfterPlot(villianArrive);
			var heroTravelTime = Locations.GetTravelTime(heroLocation, villainLocation);

			// We need a proper epilogue at the end.
			var epilogArrive = new ArriveNode(heroLocation)
			{
				NodeType = NodeType.Epilogue
			};
			var epilogue = new EpilogueNode(heroLocation, hero)
			{
				NodeType = NodeType.Epilogue
			};
			var epilogueTravelTime = Locations.GetTravelTime(villainLocation, heroLocation);

			// These two plots have to connect some how. In this case, we are
			// going to have a fight between the two which will result in the
			// hero winning.
			var beginFight = new BeginFightNode(villainLocation);
			var death = new DeathNode(villainLocation, villain)
			{
				NodeType = NodeType.Epilogue
			};
			var endFight = new EndFightNode(villainLocation)
				{
					NodeType = NodeType.Epilogue
				}
				.AddAfterPlot(death)
				.AddBeforePlot(epilogArrive);

			// We need to link all of these plots together. The hero is born,
			// the leave for the villain, show up, and then they and the Big
			// Bad fight it out.
			Graph.Connect(villianBorn, villianLeave, new PlotTimeSpan(villain.GetAge), villain);
			Graph.Connect(villianLeave, villianArrive, villain);
			Graph.Connect(villianArrive, beginFight, villain);

			Graph.Connect(introduceHero, inciting, new PlotTimeSpan(hero.GetAge), hero);
			Graph.Connect(inciting, heroLeave, hero);
			Graph.Connect(heroLeave, heroArrive, heroTravelTime, hero);
			Graph.Connect(heroArrive, beginFight, hero);

			Graph.Connect(beginFight, death, villain);
			Graph.Connect(beginFight, endFight, hero);
			Graph.Connect(endFight, epilogArrive, epilogueTravelTime, hero);
			Graph.Connect(epilogArrive, epilogue, hero);

			// Add all the plots into the plot manager.
			Nodes.InitialNode = inciting;
			Nodes.Add(villianBorn, villianArrive, villianLeave, introduceHero);
			Nodes.Add(inciting, beginFight, endFight);
			Nodes.Add(heroLeave, heroArrive);
			Nodes.Add(death, epilogArrive, epilogue);
		}
	}
}
