#region Namespaces

using GirlKillsDragon.Nodes;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots.Validators
{
	/// <summary>
	/// Validates that every plot connected to the initial plot has been added
	/// to the PlotManager.
	/// </summary>
	public class ConnectedNodeValidator : IValidator
	{
		private readonly NodeGraph _graph;

		private readonly ILogger _log;

		private readonly NodeManager _nodes;

		public ConnectedNodeValidator(
			ILogger log,
			NodeManager nodes,
			NodeGraph graph)
		{
			_log = log;
			_nodes = nodes;
			_graph = graph;
		}

		public bool Validate()
		{
			// Report what we're doing.
			_log.Verbose("Validating all connected plots have been added to PlotManager");

			// Create a visitor and go through every node looking for missing plots.
			var errors = false;
			var visitor = new GraphVisitor
			{
				VisitNode = delegate(Node node)
				{
					if (_nodes.Contains(node))
					{
						return;
					}

					_log.Error("The node {0} has not been added to the plots", node);
					errors = true;
				}
			};

			visitor.Visit(_graph, _nodes.InitialNode);

			// Return if we haven't found errors.
			return !errors;
		}
	}
}
