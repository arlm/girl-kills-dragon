#region Namespaces

using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Plots.Validators
{
	public class ValidatorManager : IRegisteredManager
	{
		private readonly List<IValidator> _validators;

		public ValidatorManager(IEnumerable<IValidator> validators)
		{
			_validators = validators.ToList();
		}

		public bool Validate()
		{
			var valid = _validators.All(v => v.Validate());

			return valid;
		}
	}
}
