#region Namespaces

using System.Linq;
using GirlKillsDragon.Nodes;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots.Validators
{
	/// <summary>
	/// Validates the actors are properly moved from plot to plot and don't
	/// split across multiple links.
	/// </summary>
	public class PlotActorValidator : IValidator
	{
		private readonly NodeGraph _graph;

		private readonly ILogger _log;

		private readonly NodeManager _nodes;

		public PlotActorValidator(
			ILogger log,
			NodeManager nodes,
			NodeGraph graph)
		{
			_log = log;
			_nodes = nodes;
			_graph = graph;
		}

		public bool Validate()
		{
			// Report what we're doing.
			var entryPlots = _nodes.EntryNodes.ToList();

			_log.Verbose(
				"Validating actors starting with {0:N0} plots",
				entryPlots.Count);

			// Go through each of the entry plots, which should only be
			// BornPlot instances.
			var errors = false;

			foreach (var first in entryPlots)
			{
				// Make sure we're an entry plot.
				var entry = first as ActorNodeBase;

				if (!first.IsEntry || entry == null)
				{
					_log.Error("The initial node did not extend ActorPlotBase or is not marked as an entry: {0}", first);
					errors = true;
					continue;
				}

				// Create a visitor starting with the first plot and look for a consistent path. We only
				// move forward.
				var actor = entry.Actor;
				Node lastNode = null;
				var visitor = new GraphVisitor
				{
					BackwardEdgeFilter = EdgeFilter.ExcludeAll,
					ForwardEdgeFilter = EdgeFilter.Follow(actor).SetIncludeAll(),
					VisitNode = delegate(Node node) { lastNode = node; },
					VisitForwardEdges = delegate(EndpointEdgeCollection edges)
					{
						if (edges.Count <= 1)
						{
							return;
						}

						_log.Error("The actor {0} is leaving multiple plots: {1}", actor, edges);
						errors = true;
					}
				};

				visitor.Visit(_graph, entry);

				// Verify our last node.
				if (lastNode == null)
				{
					_log.Error("The actor {0} is not in any plots", actor);
					errors = true;
					continue;
				}

				if (!lastNode.IsExit)
				{
					_log.Error("The actor {0}'s last node {1} is not an exit plot", actor, lastNode);
					errors = true;
				}
			}

			// Return true if we had no errors.
			return !errors;
		}
	}
}
