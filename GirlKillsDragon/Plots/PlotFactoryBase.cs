#region Namespaces

using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots
{
	public abstract class PlotFactoryBase
	{
		public ActorManager Actors { get; set; }

		public Chaos Chaos { get; set; }

		public NodeGraph Graph { get; set; }

		public LocationManager Locations { get; set; }

		public ILogger Log { get; set; }

		public NodeManager Nodes { get; set; }

		public Project Project { get; set; }
	}
}
