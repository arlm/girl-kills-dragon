#region Namespaces

using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	/// <summary>
	/// A plot injector that creates a new actor that follows another one.
	/// </summary>
	public class NewFollowerPlotInjector : PlotInjectorBase
	{
		public override bool Inject(int plotId, Edge edge)
		{
			// Make sure we have at least one actor.
			if (edge.Actors.Count == 0)
			{
				return false;
			}

			// Figure out which character the new one wants to follow. We don't need a filter with
			// this one because they can follow anyone, including the main character.
			var followActor = Chaos.Choose(edge.Actors);

			// We have a new character joining the party.
			var actor = Actors.Create();

			// We use the arrive plot to have the opponent show up from nowhere.
			// This avoids wasting the effort to build up their backstory (these
			// are mooks). Once they are in the location, they pick a fight.
			var bornLocation = Locations.Create();
			var location = edge.TailNode.Location;
			var born = new BornNode(bornLocation, actor)
			{
				NodeType = NodeType.Past
			};
			var leave = new LeaveNode(bornLocation)
			{
				NodeType = NodeType.Past
			};
			var introduce = new ArriveNode(location);
			var join = new MergeGroupNode(location, edge.Actors, new[] {actor});

			Nodes.Add(born, leave, introduce, join);

			// Make some noise.
			Log.Debug(
				"{2}: New Actor: {0} joins {1}",
				actor,
				followActor,
				edge);

			// Remove the existing link because we're going to rebuild it.
			Graph.Disconnect(edge);

			// Inject the next link into the graph. We only include the original party until we use
			// the Graph to merge the group following the main character.
			Graph.Connect(born, leave, actor);
			Graph.Connect(leave, introduce, actor);
			Graph.Connect(introduce, join, actor);
			Graph.Connect(edge.HeadNode, join, edge.Actors);
			Graph.Connect(join, edge.TailNode, edge.Actors);

			Graph.ActorFollowsActor(join, followActor, actor);

			// We are successful.
			return true;
		}
	}
}
