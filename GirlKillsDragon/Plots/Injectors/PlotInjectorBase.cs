#region Namespaces

using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	/// <summary>
	/// An instance that inserts one or more plots into the directed graph,
	/// adding complexity to the story.
	/// </summary>
	public abstract class PlotInjectorBase : PlotFactoryBase
	{
		/// <summary>
		/// Gets the filter associated with this injector. This defaults to the inner scene filter since
		/// that is the most common type for injectors.
		/// </summary>
		public virtual EdgeFilter EdgeFilter => EdgeFilter.InnerSceneFilter;

		/// <summary>
		/// Attempts to inject the plots into the current edge (which is already filtered by EdgeFilter).
		/// </summary>
		/// <param name="plotId">The unique identifier for the plot being added.</param>
		/// <param name="edge">The link to insert the plot elements.</param>
		/// <returns>True if it could be injected, otherwise false.</returns>
		public abstract bool Inject(int plotId, Edge edge);
	}
}
