#region Namespaces

using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	/// <summary>
	/// Injects a random obstacleinto the story.
	/// </summary>
	public class TrialPlotInjector
		: InsertNodePlotInjectorBase<BeginTrialNode, EndTrialNode>
	{
		public override string PluralMessage => "encounter an obstacle";

		public override string SingularMessage => "encounters an obstacle";
	}
}
