#region Namespaces

using System.Linq;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Extensions;
using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	/// <summary>
	/// Makes a death far worse by adding another death before one of them.
	/// </summary>
	public class AnotherDiesPlotInjector : PlotInjectorBase
	{
		public override EdgeFilter EdgeFilter => new EdgeFilter
		{
			HeadFilter = NodeFilter.IncludeAll
				.SetIsFiltered(node => node.InActors.Count <= 2),
			TailFilter = NodeFilter.IncludeAll
				.SetIsFiltered(node => !(node is DeathNode))
		};

		public override bool Inject(int plotId, Edge edge)
		{
			// Get the death node we are working from.
			var previousDeath = (DeathNode) edge.TailNode;
			var previousActor = previousDeath.Actor;

			// Figure out who else is going to die.
			var victims = edge.HeadNode.InActors
				.Where(a => a != previousActor)
				.Filter(new ActorFilter())
				.ToList();

			if (victims.Count == 0)
			{
				// We don't have a valid actor to remove.
				return false;
			}

			// We have a potential victim now.
			var victim = Chaos.Choose(victims);
			var victimDeath = new DeathNode(edge.HeadNode.Location, victim)
				.AddBeforePlot(previousDeath);

			Nodes.Add(victimDeath);

			// Make some noise.
			Log.Debug(
				"{0}: Another Death: When {1} dies, so does {2}",
				edge,
				previousDeath.Actor,
				victim);

			// Add in this code, but don't disconnect the links. We do have to
			// remove the victim from the previous connections.
			Graph.Remove(victim, edge.HeadNode);
			Graph.Connect(edge.HeadNode, victimDeath, victim);

			// Let's add another death.
			return true;
		}
	}
}
