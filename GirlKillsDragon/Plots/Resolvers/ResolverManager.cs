#region Namespaces

using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Plots.Resolvers
{
	public class ResolverManager : IRegisteredManager
	{
		private readonly List<IInjectionResolver> _injectionResolvers;

		private readonly List<IPlotResolver> _plotResolvers;

		public ResolverManager(
			IEnumerable<IPlotResolver> plotResolvers,
			IEnumerable<IInjectionResolver> injectionResolvers)
		{
			_plotResolvers = plotResolvers.OrderBy(r => r.ResolverOrder).ToList();
			_injectionResolvers = injectionResolvers.OrderBy(r => r.ResolverOrder).ToList();
		}

		public void ResolveInjection()
		{
			foreach (var resolver in _injectionResolvers)
			{
				resolver.Resolve();
			}
		}

		public void ResolvePlot()
		{
			foreach (var resolver in _plotResolvers)
			{
				resolver.Resolve();
			}
		}
	}
}
