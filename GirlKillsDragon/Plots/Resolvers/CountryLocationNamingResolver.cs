#region Namespaces

using System.Collections.Generic;
using GirlKillsDragon.Details;
using GirlKillsDragon.Locations;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots.Resolvers
{
	public class CountryLocationNamingResolver : IPlotResolver
	{
		private readonly Chaos _chaos;

		private readonly CountryManager _countries;

		private readonly CountryResolver _countryResolver;

		private readonly FileSystem _fileSystem;

		private readonly LocationManager _locations;

		private readonly ILogger _log;

		public CountryLocationNamingResolver(
			ILogger log,
			Chaos chaos,
			LocationManager locations,
			CountryManager countries,
			CountryResolver countryResolver,
			FileSystem fileSystem)
		{
			_log = log;
			_chaos = chaos;
			_locations = locations;
			_countries = countries;
			_countryResolver = countryResolver;
			_fileSystem = fileSystem;
		}

		public bool Enabled { get; set; } = true;

		public int ResolverOrder => _countryResolver.ResolverOrder + 1;

		public void Resolve()
		{
			// If we aren't enabled (for tests), we don't resolve. This gives
			// us a consistent testing without the random naming breaking it
			// as the inputs change.
			if (!Enabled)
			{
				return;
			}

			// Set up the naming.
			var countryFile = _fileSystem.GetDataFile("countries.txt");
			var usLastFile = _fileSystem.GetDataFile("en.last.txt");
			var jpLastFile = _fileSystem.GetDataFile("jp.last.txt");

			var chain = new MarkovChainFactory(_chaos)
				.SetIgnoreWeights()
				.SetWeight(5)
				.Append(usLastFile)
				.SetWeight(1)
				.Append(jpLastFile)
				.SetSingleToken()
				.SetForceUnique()
				.SetWeight(10)
				.Append(countryFile)
				.Create();

			// Go through the locations and name each one based on the country.
			foreach (var location in _locations)
			{
				var name = chain.Create();

				location.Noun
					.AddDetails("name", new Detail(DetailType.Is, name));
			}
		}

		private string GetGivenName(
			MarkovChain chain,
			HashSet<string> seen,
			HashSet<string> seenLetter)
		{
			// Loop forever until we find a valid combination.
			var attempt = 0;

			while (true)
			{
				// Generate a random name.
				var name = chain.Create();

				// If we've seen the name already, always ignore it.
				if (seen.Contains(name))
				{
					continue;
				}

				// See if we've seen the first letter, we'll try a number of
				// times before giving up.
				var first = name[0].ToString();

				if (seenLetter.Contains(first))
				{
					if (++attempt > 10)
					{
						attempt = 0;
						seenLetter.Clear();
					}

					continue;
				}

				// Mark that we've accept this so mark it for repeat.
				seen.Add(name);
				seenLetter.Add(first);

				return name;
			}
		}
	}
}
