namespace GirlKillsDragon.Plots.Resolvers
{
	/// <summary>
	/// Indicates a resolver that is run after the injections have completed.
	/// </summary>
	public interface IPlotResolver : IResolver
	{
	}
}
