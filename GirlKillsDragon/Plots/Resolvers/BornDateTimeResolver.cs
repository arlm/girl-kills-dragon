#region Namespaces

using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Resolvers
{
	/// <summary>
	/// Resolves all the dates of birth from the starting point of the novel.
	/// </summary>
	public class BornDateTimeResolver : IInjectionResolver
	{
		private readonly NodeManager _nodes;

		private readonly RemoveOrphanActorsResolver _removeOrphanActorsResolver;

		public BornDateTimeResolver(
			NodeManager nodes,
			RemoveOrphanActorsResolver removeOrphanActorsResolver)
		{
			_nodes = nodes;
			_removeOrphanActorsResolver = removeOrphanActorsResolver;
		}

		public int ResolverOrder => _removeOrphanActorsResolver.ResolverOrder + 1;

		public void Resolve()
		{
			foreach (var node in _nodes.BornNodes)
			{
				var actor = node.Actor;
				var bornDateTime = _nodes.InitialNode.DateTime.Resolved - actor.Age;

				node.DateTime.Specific = node.DateTime.Resolved = bornDateTime;
			}
		}
	}
}
