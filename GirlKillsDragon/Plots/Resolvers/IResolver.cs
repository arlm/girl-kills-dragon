namespace GirlKillsDragon.Plots.Resolvers
{
	public interface IResolver
	{
		/// <summary>
		/// Gets the order that resolution must be done. The lowest is resolved first.
		/// </summary>
		int ResolverOrder { get; }

		void Resolve();
	}
}
