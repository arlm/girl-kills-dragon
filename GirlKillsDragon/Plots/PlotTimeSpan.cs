#region Namespaces

using System;

#endregion

namespace GirlKillsDragon.Plots
{
	/// <summary>
	/// Represents a passage of time in relative terms.
	/// </summary>
	public class PlotTimeSpan
	{
		private readonly Func<TimeSpan> _get;

		public PlotTimeSpan()
			: this(TimeSpan.FromMinutes(1))
		{
		}

		public PlotTimeSpan(TimeSpan minimum)
			: this(() => minimum)
		{
		}

		public PlotTimeSpan(Func<TimeSpan> get)
		{
			_get = get;
		}

		public TimeSpan Get()
		{
			return _get();
		}
	}
}
