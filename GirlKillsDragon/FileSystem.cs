#region Namespaces

#endregion

#region Namespaces

using System;
using System.Collections.Generic;
using System.IO;

#endregion

namespace GirlKillsDragon
{
	/// <summary>
	/// Provides a consistent access to the filesystem used for generating the code.
	/// </summary>
	public class FileSystem
	{
		private DirectoryInfo _directory;

		public DirectoryInfo DataDirectory => new DirectoryInfo(Path.Combine(Directory.FullName, "Data"));

		public DirectoryInfo DetailsDirectory => new DirectoryInfo(Path.Combine(DataDirectory.FullName, "Details"));

		public DirectoryInfo Directory
		{
			get
			{
				// If we have a directory, just use that.
				if (_directory != null)
				{
					return _directory;
				}

				// If we don't, attempt to find it.
				var directory = new DirectoryInfo(Environment.CurrentDirectory);

				while (directory != null && directory.GetDirectories("Data").Length == 0)
				{
					directory = directory.Parent;
				}

				// If we have a null directory, we couldn't find it.
				_directory = directory
					?? throw new DirectoryNotFoundException("Could not file directory containing the Data/ directory.");

				return _directory;
			}
			set => _directory = value;
		}

		public FileInfo GetDataFile(string fileName)
		{
			return new FileInfo(Path.Combine(DataDirectory.FullName, fileName));
		}

		public IEnumerable<FileInfo> GetDetailFiles()
		{
			return DetailsDirectory.GetFiles("*.yaml");
		}
	}
}
