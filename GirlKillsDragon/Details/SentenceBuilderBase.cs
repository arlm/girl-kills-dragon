namespace GirlKillsDragon.Details
{
	public abstract class SentenceBuilderBase
	{
		/// <summary>
		/// Writes out a single sentence based on the input.
		/// </summary>
		/// <returns></returns>
		public abstract string Write(WriterContext writerContext);
	}
}
