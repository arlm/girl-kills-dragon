#region Namespaces

using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Details
{
	/// <summary>
	/// Defines a group of unordered tags that can be used to test for comparison
	/// with another set of tags.
	/// </summary>
	public class TagSet : HashSet<string>
	{
		public TagSet()
		{
		}

		public TagSet(string tags)
			: base(tags.Split(' ').Select(t => t.ToLower().Trim()))
		{
		}

		public TagSet(IEnumerable<string> tags)
		{
			foreach (var tag in tags)
			{
				Add(tag);
			}
		}

		public TagSet(TagSet tags)
			: base(tags)
		{
		}

		public bool IsMatch(string tags)
		{
			return IsMatch(new TagSet(tags));
		}

		public bool IsMatch(TagSet tags)
		{
			if (Count == 0)
			{
				return false;
			}

			return this.All(tags.Contains);
		}

		public override string ToString()
		{
			return string.Join(" ", this.OrderBy(t => t));
		}

		public TagSet Union(TagSet tags)
		{
			return new TagSet(new List<string>(this).Union(tags));
		}

		public TagSet Union(string tags)
		{
			var tagSet = new TagSet(tags);

			return Union(tagSet);
		}
	}
}
