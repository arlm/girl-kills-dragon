namespace GirlKillsDragon.Details
{
	public class NounSelectorDetail
	{
		public NounSelectorDetail(string templateTags, string detailTags)
			: this(new TagSet(templateTags), new TagSet(detailTags))
		{
		}

		public NounSelectorDetail(TagSet templateTags, TagSet detailTags)
		{
			TemplateTags = templateTags;
			DetailTags = detailTags;
		}

		public TagSet DetailTags { get; }

		public TagSet TemplateTags { get; }

		public override string ToString()
		{
			return $"NounSelectorDetail(Template={TemplateTags}, Detail={DetailTags})";
		}
	}
}
