#region Namespaces

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Extensions;

#endregion

namespace GirlKillsDragon.Details
{
	/// <summary>
	/// Manages the various detail sets that are loaded into the system.
	/// </summary>
	public class DetailSetManager : IEnumerable<DetailSet>
	{
		private readonly Chaos _chaos;

		private readonly DetailSetCollection _detailSets = new DetailSetCollection();

		public DetailSetManager(Chaos chaos)
		{
			_chaos = chaos;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<DetailSet> GetEnumerator()
		{
			return _detailSets.GetEnumerator();
		}

		public void Add(DetailSet detailSet)
		{
			_detailSets.Add(detailSet);
		}

		public DetailSet Choose(TagSet tags)
		{
			var choices = _detailSets.Where(ds => tags.IsMatch(ds.Tags)).ToList();

			if (choices.Count == 0)
			{
				return null;
			}

			var choice = choices.Choose(_chaos);
			return choice;
		}
	}
}
