namespace GirlKillsDragon.Details
{
	public class Detail
	{
		public Detail(DetailType detailType, string value)
		{
			DetailType = detailType;
			Value = value;
		}

		public DetailType DetailType { get; set; }

		public string Value { get; set; }

		public override string ToString()
		{
			return $"{DetailType} {Value}";
		}
	}
}
