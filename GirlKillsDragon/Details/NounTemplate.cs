#region Namespaces

using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Details
{
	/// <summary>
	/// Defines template of how a noun is organized with various sub-nouns. This is how a "human"
	/// has "hair". In another way, these are the "slots" that describe how the noun is
	/// arrange and what can be accessed.
	/// </summary>
	public class NounTemplate
		: ITaggedItem
	{
		/// <summary>
		/// Constructs a template with a given set of tags and an ordered list of subtags
		/// which will be resolved in the order given.
		/// </summary>
		public NounTemplate(string tags, params string[] subTags)
		{
			Tags = new TagSet(tags);
			SubTags = new List<TagSet>(subTags.Select(t => new TagSet(t)));
		}

		/// <summary>
		/// Gets or sets a flag whether this template is shared across all instances in a given
		/// noun.
		/// </summary>
		public bool Global { get; set; }

		public List<TagSet> SubTags { get; set; }

		public TagSet Tags { get; set; }
	}
}
