#region Namespaces

#endregion

namespace GirlKillsDragon.Details
{
	public class NounTemplateManager : TaggedManagerBase<NounTemplate, NounTemplateCollection>
	{
		public NounTemplateManager(Chaos chaos)
			: base(chaos)
		{
		}
	}
}
