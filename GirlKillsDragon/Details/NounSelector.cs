#region Namespaces

using System.Collections;
using System.Collections.Generic;

#endregion

namespace GirlKillsDragon.Details
{
	/// <summary>
	/// Describes what details are chosen for a specific noun. This can be for a higher-level
	/// detail or 
	/// </summary>
	public class NounSelector : IEnumerable<NounSelectorDetail>, ITaggedItem
	{
		private readonly List<NounSelectorDetail> _details = new List<NounSelectorDetail>();

		public NounSelector(string selectorTags, string tags)
			: this(selectorTags, new TagSet(tags))
		{
		}

		public NounSelector(string selectorTags, TagSet tags)
		{
			SelectorTags = new TagSet(selectorTags);
			Tags = tags;
		}

		public TagSet SelectorTags { get; set; }

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<NounSelectorDetail> GetEnumerator()
		{
			return _details.GetEnumerator();
		}

		public TagSet Tags { get; set; }

		public void Add(NounSelectorDetail item)
		{
			_details.Add(item);
		}
	}
}
