namespace GirlKillsDragon.Details
{
	public class IsSentenceBuilder : SentenceBuilderBase
	{
		public override string Write(WriterContext writerContext)
		{
			// Figure out what we're going to be writing about.
			var subject = writerContext.Subject;
			var detail = writerContext.GetDetail(DetailType.Is);

			// Write it out.
			var sent = $"{subject.Name} was {detail.Value}.";

			return sent;
		}
	}
}
