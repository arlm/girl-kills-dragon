#region Namespaces

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Extensions;

#endregion

namespace GirlKillsDragon.Details
{
	public abstract class TaggedManagerBase<TItem, TCollection> : IEnumerable<TItem>
		where TItem : ITaggedItem
		where TCollection : IList<TItem>, new()
	{
		private readonly Chaos _chaos;

		private readonly TCollection _items = new TCollection();

		public TaggedManagerBase(Chaos chaos)
		{
			_chaos = chaos;
		}

		public IEnumerator<TItem> GetEnumerator()
		{
			return _items.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void Add(TItem item)
		{
			_items.Add(item);
		}

		public TItem Choose(string tags)
		{
			return Choose(new TagSet(tags));
		}

		public TItem Choose(TagSet tags)
		{
			var choices = _items
				.Where(t => tags.IsMatch(t.Tags))
				.ToList();
			var choice = choices.Choose(_chaos);

			return choice;
		}
	}
}
