#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Humanizer;

#endregion

namespace GirlKillsDragon.Details
{
	/// <summary>
	/// A string-based Markov chain with support for weighted values and a given number of ranks.
	/// </summary>
	public class MarkovChain
	{
		private readonly Chaos _chaos;

		private readonly HashSet<string> _exclude = new HashSet<string>();

		private readonly Dictionary<string, MarkovLink> _links = new Dictionary<string, MarkovLink>();

		private readonly int _order;

		private readonly List<Func<string, string>> _replacements = new List<Func<string, string>>();

		public MarkovChain(Chaos chaos, int order)
		{
			_chaos = chaos;
			_order = order;
		}

		public bool Titleize { get; set; } = true;

		/// <summary>
		/// Adds the break down of the input as single-character markov chains using the given weights.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="weight"></param>
		public void Add(string input, double weight = 1.0)
		{
			// Add a number of nulls at the end of the input equal to our order to handle end-of-name. We use the "_" to represent
			// a terminal link.
			input += new string('_', _order - 1);

			// Create an empty list of items that is one less than _order. We will use this as the lookup key for the chain
			// resolution. We use '_' for the leading of the chain.
			var search = Enumerable.Range(0, _order - 1).Select(i => '_').ToList();

			// Go through the input and add each one.
			foreach (var t in input)
			{
				// Add in this item into the chain.
				Add(search, t, weight);

				// Push the item into the chain and pop off the last one.
				search.Add(t);
				search.RemoveAt(0);
			}
		}

		/// <summary>
		/// Adds in the search/next chain into the list.
		/// </summary>
		private void Add(List<char> search, char next, double weight)
		{
			// See if we've already got this chain.
			var searchString = string.Join("", search);

			if (!_links.TryGetValue(searchString, out var link))
			{
				link = new MarkovLink();
				_links[searchString] = link;
			}

			// Now that we have the correct link, add this chain.
			link.Add(next, weight);
		}

		public void AddReplacement(Func<string, string> replacement)
		{
			_replacements.Add(replacement);
		}

		/// <summary>
		/// Creates a name based on the Markov chain.
		/// </summary>
		/// <returns></returns>
		public string Create()
		{
			while (true)
			{
				// Create a word.
				var attempt = CreateAttempt();

				// In general, we don't like more than eight characters.
				if (attempt.Length > 10)
				{
					continue;
				}

				// Otherwise, if we are excluding the value, keep going.
				if (_exclude.Contains(attempt))
				{
					continue;
				}

				if (Titleize)
				{
					attempt = attempt.Titleize();
				}

				foreach (var replacement in _replacements)
				{
					attempt = replacement(attempt);
				}

				return attempt;
			}
		}

		private string CreateAttempt()
		{
			// Create an empty list of items that is one less than _order. We will use this as the lookup key for the chain
			// resolution. We use '_' for the leading of the word.
			var search = Enumerable.Range(0, _order - 1).Select(i => '_').ToList();

			// Loop until we get to the end of the word.
			var buffer = new StringBuilder();

			while (true)
			{
				// Grab the next link which has all the potential values going out.
				var searchString = string.Join("", search);
				var link = _links[searchString];

				// Get the next character from the link.
				var next = link.Next(_chaos);

				// If this is a terminal, then we're done.
				if (next == '_')
				{
					return buffer.ToString();
				}

				// Otherwise, add it and move on.
				buffer.Append(next);
				search.Add(next);
				search.RemoveAt(0);
			}
		}

		public void Exclude(string name)
		{
			_exclude.Add(name);
		}

		private class MarkovLink
		{
			private readonly Dictionary<char, double> _nexts = new Dictionary<char, double>();

			private double _weight;

			public void Add(char next, double weight)
			{
				// See if we already have this next character. We don't care if we have it since we'll be putting it back.
				_nexts.TryGetValue(next, out var nextWeight);

				// Add the weight and put it back.
				nextWeight += weight;
				_weight += weight;

				_nexts[next] = nextWeight;
			}

			public char Next(Chaos chaos)
			{
				// If we only have one next, then return it.
				if (_nexts.Count == 1)
				{
					return _nexts.Keys.First();
				}

				// Otherwise, pick a random one. We use the internal `_weight` to figure out our maximum value for a weighted
				// value, then loop through the keys (in order) until we find the correct one.
				var pickWeight = chaos.NextDouble() * _weight;
				var currentWeight = 0.0;

				foreach (var key in _nexts.Keys.OrderBy(k => k))
				{
					currentWeight += _nexts[key];

					if (currentWeight >= pickWeight)
					{
						return key;
					}
				}

				// We should never get here.
				throw new InvalidOperationException("Cannot pick the next value for the key.");
			}
		}
	}
}
