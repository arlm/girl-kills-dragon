#region Namespaces

using System;
using GirlKillsDragon.Details;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Actors
{
	/// <summary>
	/// Describes a unique entity within the system. An actor can be the main character or a monster in the story.
	/// 
	/// This is the core information about the actor which is modified by progression through the various plots.
	/// </summary>
	public class Actor : IComparable<Actor>, IEquatable<Actor>
	{
		public Actor()
			: this(-1)
		{
		}

		public Actor(int actorId)
		{
			ActorId = actorId;
			Noun = new Noun();
		}

		public int ActorId { get; }

		public TimeSpan Age { get; set; } = TimeSpan.FromDays(365 * 16);

		/// <summary>
		/// The node where the character is first created.
		/// </summary>
		public BornNode Born { get; set; }

		/// <summary>
		/// The country where the character is born.
		/// </summary>
		public Country Country => Born.Location.Country;

		public string FirstName => Noun.GetValue("name.first");

		public string LastName => Noun.GetValue("name.last");

		public string Name => Noun.GetValue("name");

		public Noun Noun { get; set; }

		public string Pronoun => Noun.GetValue("pronoun");

		/// <summary>
		/// Gets or sets a flag whether this actor is significant to the plot.
		/// </summary>
		public bool Significant { get; set; }

		public int CompareTo(Actor other)
		{
			return ActorId.CompareTo(other.ActorId);
		}

		public bool Equals(Actor other)
		{
			if (ReferenceEquals(null, other))
			{
				return false;
			}
			if (ReferenceEquals(this, other))
			{
				return true;
			}

			return ActorId == other.ActorId;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}
			if (ReferenceEquals(this, obj))
			{
				return true;
			}
			if (obj.GetType() != GetType())
			{
				return false;
			}

			return Equals((Actor) obj);
		}

		/// <summary>
		/// Function to retrieve the age of the character if it changes.
		/// </summary>
		public TimeSpan GetAge()
		{
			return Age;
		}

		public override int GetHashCode()
		{
			return ActorId;
		}

		public static bool operator ==(Actor left, Actor right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(Actor left, Actor right)
		{
			return !Equals(left, right);
		}

		public Actor SetSignificant(bool significant = true)
		{
			Significant = significant;
			return this;
		}

		public override string ToString()
		{
			return $"A{ActorId}";
		}
	}
}
