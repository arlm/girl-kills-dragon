#region Namespaces

#endregion

namespace GirlKillsDragon.Actors
{
	/// <summary>
	/// A description of a filter that is used to select actors in a list.
	/// </summary>
	public class ActorFilter
	{
		/// <summary>
		///  If set to true (the default), then exclude actors with the `Significant` flag set.
		/// </summary>
		public bool DenySignificant { get; set; } = true;

		public bool Exclude(Actor actor)
		{
			return !Include(actor);
		}

		public bool Include(Actor actor)
		{
			if (DenySignificant && actor.Significant)
			{
				return false;
			}

			return true;
		}
	}
}
