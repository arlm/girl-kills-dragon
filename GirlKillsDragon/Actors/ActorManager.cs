#region Namespaces

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Details;

#endregion

namespace GirlKillsDragon.Actors
{
	/// <summary>
	/// A manager class that handles creating and coordinating various actors within a plot.
	/// </summary>
	public class ActorManager : IRegisteredManager, IEnumerable<Actor>
	{
		private readonly List<Actor> _actors = new List<Actor>();

		private readonly Chaos _chaos;

		private readonly NounFactory _nounFactory;

		private int _nextActorId;

		public ActorManager(
			Chaos chaos,
			NounFactory nounFactory)
		{
			_chaos = chaos;
			_nounFactory = nounFactory;
		}

		public int Count => _actors.Count;

		public Actor Primary => _actors.First(a => a.ActorId == 0);

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<Actor> GetEnumerator()
		{
			return _actors.GetEnumerator();
		}

		public Actor Create(string selector = "human")
		{
			var actor = new Actor(_nextActorId++)
			{
				Age = TimeSpan.FromDays(365 * (16 + 5 * _chaos.NextDouble())),
				Noun = _nounFactory.Create(selector)
			};

			_actors.Add(actor);

			return actor;
		}

		public void Remove(IEnumerable<Actor> actors)
		{
			var act = actors.ToList();

			_actors.RemoveAll(a => act.Contains(a));
		}
	}
}
