#region Namespaces

using GirlKillsDragon.Locations;

#endregion

namespace GirlKillsDragon.Nodes
{
	public class EndFightNode : Node
	{
		public EndFightNode(Location location)
			: base(location)
		{
		}
	}
}
