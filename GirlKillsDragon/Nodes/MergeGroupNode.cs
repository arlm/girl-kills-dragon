#region Namespaces

using System.Collections.Generic;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// A plot where two groups merge together.
	/// </summary>
	public class MergeGroupNode : Node
	{
		public MergeGroupNode(
			Location location,
			IEnumerable<Actor> group,
			IEnumerable<Actor> joining)
			: base(location)
		{
			Group = new ActorCollection(group);
			Joining = new ActorCollection(joining);
		}

		public ActorCollection Group { get; set; }

		public ActorCollection Joining { get; set; }
	}
}
