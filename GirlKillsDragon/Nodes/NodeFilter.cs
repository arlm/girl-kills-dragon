#region Namespaces

#endregion

#region Namespaces

using System;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// Defines a filtering class that determines if a node should be filtered out.
	/// </summary>
	public class NodeFilter
	{
		public NodeFilter()
		{
		}

		public NodeFilter(NodeFilter filter)
		{
			DenyArrive = filter.DenyArrive;
			DenyEntry = filter.DenyEntry;
			DenyEpilogue = filter.DenyEpilogue;
			DenyExit = filter.DenyExit;
			DenyFuture = filter.DenyFuture;
			DenyInitial = filter.DenyInitial;
			DenyLeave = filter.DenyLeave;
			DenyNarrative = filter.DenyNarrative;
			DenyPast = filter.DenyPast;
			IsFiltered = filter.IsFiltered;
			RequireArrive = filter.RequireArrive;
			RequireCanInjectAfter = filter.RequireCanInjectAfter;
			RequireLeave = filter.RequireLeave;
		}

		public bool DenyArrive { get; set; }

		public bool DenyEntry { get; set; }

		public bool DenyEpilogue { get; set; } = true;

		public bool DenyExit { get; set; }

		public bool DenyFuture { get; set; } = true;

		public bool DenyInitial { get; set; } = true;

		public bool DenyLeave { get; set; }

		public bool DenyNarrative { get; set; }

		public bool DenyPast { get; set; } = true;

		/// <summary>
		/// A filter that excludes all nodes.
		/// </summary>
		public static NodeFilter ExcludeAll => new NodeFilter
		{
			IsFiltered = node => true
		};

		/// <summary>
		/// A filter that includes all nodes in the system.
		/// </summary>
		public static NodeFilter IncludeAll => new NodeFilter
		{
			DenyEpilogue = false,
			DenyFuture = false,
			DenyPast = false,
			DenyInitial = false
		};

		/// <summary>
		/// Create a custom test function for the node. If the return value is true, then
		/// the item is filtered out.
		/// </summary>
		public Func<Node, bool> IsFiltered { get; set; } = node => false;

		public bool RequireArrive { get; set; }

		public bool RequireCanInjectAfter { get; set; }

		public bool RequireLeave { get; set; }

		public bool Exclude(Node node)
		{
			return !Include(node);
		}

		public bool Include(Node node)
		{
			if (IsFiltered(node))
			{
				return false;
			}

			if (node.IsArrive)
			{
				if (DenyArrive)
				{
					return false;
				}
			}
			else if (RequireArrive)
			{
				return false;
			}

			if (node.IsLeave)
			{
				if (DenyLeave)
				{
					return false;
				}
			}
			else if (RequireLeave)
			{
				return false;
			}

			if (DenyExit & node.IsExit)
			{
				return false;
			}

			if (DenyEntry && node.IsEntry)
			{
				return false;
			}

			if (DenyInitial && node.IsInitial)
			{
				return false;
			}

			if (DenyPast && node.NodeType == NodeType.Past)
			{
				return false;
			}

			if (DenyFuture && node.NodeType == NodeType.Future)
			{
				return false;
			}

			if (DenyNarrative && node.NodeType == NodeType.Narrative)
			{
				return false;
			}

			if (DenyEpilogue && node.NodeType == NodeType.Epilogue)
			{
				return false;
			}

			if (RequireCanInjectAfter && !node.CanInjectAfter)
			{
				return false;
			}

			// Everything else is okay.
			return true;
		}

		public NodeFilter SetIsFiltered(Func<Node, bool> isFiltered)
		{
			IsFiltered = isFiltered;
			return this;
		}
	}
}
