#region Namespaces

using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Plots;
using Newtonsoft.Json;

#endregion

namespace GirlKillsDragon.Nodes
{
	public class Edge
	{
		public Edge()
		{
		}

		public Edge(Node headNode, Node tailNode)
		{
			HeadNode = headNode;
			TailNode = tailNode;
		}

		public IEnumerable<string> ActorNames
		{
			get
			{
				return Actors
					.OrderBy(a => a.ActorId)
					.Select(a => a.ToString())
					.Distinct();
			}
		}

		[JsonIgnore]
		public ActorCollection Actors { get; set; }

		[JsonIgnore]
		public Node HeadNode { get; set; }

		public int? HeadNodeId => HeadNode?.NodeId;

		[JsonIgnore]
		public Node TailNode { get; set; }

		public int? TailNodeId => TailNode?.NodeId;

		public PlotTimeSpan TimeSpan { get; set; } = new PlotTimeSpan();

		public override string ToString()
		{
			return $"{HeadNode} -> {TailNode}";
		}
	}
}
