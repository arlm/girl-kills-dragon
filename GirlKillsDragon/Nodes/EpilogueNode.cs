#region Namespaces

using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;

#endregion

namespace GirlKillsDragon.Nodes
{
	public class EpilogueNode : ActorNodeBase
	{
		public EpilogueNode(Location location, Actor actor)
			: base(location, actor)
		{
		}

		public override bool IsExit => true;
	}
}
