#region Namespaces

using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;
using Newtonsoft.Json;

#endregion

namespace GirlKillsDragon.Nodes
{
	public abstract class ActorNodeBase : Node
	{
		protected ActorNodeBase(Location location, Actor actor)
			: base(location)
		{
			Actor = actor;
		}

		[JsonIgnore]
		public Actor Actor { get; }

		public int? ActorId => Actor?.ActorId;
	}
}
