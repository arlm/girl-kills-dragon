#region Namespaces

using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// Introduces a new character into the plot, establishing them in a specific plot and their values.
	/// </summary>
	public class BornNode : ActorNodeBase
	{
		public BornNode(Location location, Actor actor)
			: base(location, actor)
		{
			actor.Born = this;
		}

		public override bool IsArrive => true;

		public override bool IsEntry => true;
	}
}
