namespace GirlKillsDragon.Nodes
{
	public enum NodeType : byte
	{
		Narrative,

		/// <summary>
		/// Plots that happen before the novel officially starts. This is setup and never written out.
		/// </summary>
		Past,

		/// <summary>
		/// Plots that happen in the future, after the epilogue.
		/// </summary>
		Future,

		/// <summary>
		/// Plots that happen after the final fight but before the novel ends.
		/// </summary>
		Epilogue
	}
}
