#region Namespaces

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Serilog;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// Coordinates the various plots that have been entered into the system.
	/// </summary>
	public class NodeManager : IEnumerable<Node>, IRegisteredManager
	{
		private readonly ILogger _log;

		private readonly List<Node> _nodes = new List<Node>();

		public NodeManager(ILogger log)
		{
			_log = log;
		}

		public IEnumerable<BornNode> BornNodes => _nodes.OfType<BornNode>();

		public int Count => _nodes.Count;

		public IEnumerable<int> EntryNodeIds => EntryNodes.Select(n => n.NodeId);

		[JsonIgnore]
		public IEnumerable<Node> EntryNodes => _nodes.Where(p => p.InEdges.Count == 0);

		[JsonIgnore]
		public EpilogueNode EpilogueNode => (EpilogueNode) _nodes.FirstOrDefault(p => p is EpilogueNode);

		public int? EpilogueNodeId => EpilogueNode?.NodeId;

		public IEnumerable<int> ExitNodeIds => ExitNodes.Select(n => n.NodeId);

		[JsonIgnore]
		public IEnumerable<Node> ExitNodes => _nodes.Where(p => p.OutEdges.Count == 0);

		public int FutureCount => _nodes.Count(p => p.NodeType == NodeType.Future);

		[JsonIgnore]
		public Node InitialNode { get; set; }

		public int? InitialNodeId => InitialNode?.NodeId;

		public Node this[int index] => _nodes[index];

		public int NarrativeCount => _nodes.Count(p => p.NodeType == NodeType.Narrative);

		public int OtherCount => _nodes.Count - NarrativeCount - FutureCount - PastCount;

		public int PastCount => _nodes.Count(p => p.NodeType == NodeType.Past);

		public Project Project { get; set; }

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<Node> GetEnumerator()
		{
			return _nodes.GetEnumerator();
		}

		public void Add(params Node[] newNodes)
		{
			foreach (var node in newNodes)
			{
				if (!_nodes.Contains(node))
				{
					_log.Verbose("Adding node: {0}", node);
					_nodes.Add(node);
				}
			}
		}

		public bool Contains(Node node)
		{
			return _nodes.Contains(node);
		}

		public Node GetNode(int plotId)
		{
			return _nodes.First(p => p.NodeId == plotId);
		}

		public void Remove(Node node)
		{
			_nodes.Remove(node);
		}

		public void Remove(IEnumerable<Node> nodes)
		{
			foreach (var node in nodes)
			{
				_nodes.Remove(node);
			}
		}

		public override string ToString()
		{
			return $"{Count:N0} nodes (N {NarrativeCount:N0}, F {FutureCount:N0}, P {PastCount:N0}, O {OtherCount:N0})";
		}
	}
}
