#region Namespaces

using GirlKillsDragon.Locations;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// A plot where one or more characters leave for a new destination.
	/// </summary>
	public class LeaveNode : Node
	{
		public LeaveNode(Location location)
			: base(location)
		{
		}

		public override bool IsLeave => true;
	}
}
