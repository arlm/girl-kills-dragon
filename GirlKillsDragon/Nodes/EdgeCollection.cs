#region Namespaces

using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Nodes
{
	public class EdgeCollection : List<Edge>
	{
		public EdgeCollection()
		{
		}

		public EdgeCollection(IEnumerable<Edge> source)
			: base(source)
		{
		}

		public EdgeCollection Filter(NodeGraph graph, EdgeFilter filter)
		{
			return new EdgeCollection(this.Where(e => filter.Include(e)));
		}
	}
}
