#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Extensions
{
	public static class ListExtensions
	{
		/// <summary>
		/// Chooses a random item in a list.
		/// </summary>
		public static T Choose<T>(this IList<T> list, Chaos chaos)
		{
			return chaos.Choose(list);
		}

		/// <summary>
		/// Chooses a random item in a list.
		/// </summary>
		public static T Choose<T>(this IList<T> list, Random random)
		{
			if (list.Count == 0)
			{
				return default(T);
			}

			if (list.Count == 1)
			{
				return list[0];
			}

			var index = random.Next(list.Count);

			return list[index];
		}

		public static IEnumerable<Actor> Filter(this IEnumerable<Actor> source, ActorFilter filter)
		{
			return source.Where(filter.Include);
		}

		public static IEnumerable<Node> Filter(
			this IEnumerable<Node> source,
			NodeGraph graph,
			NodeFilter filter)
		{
			return source.Where(n => filter.Include(n));
		}

		public static IEnumerable<Edge> Filter(
			this IEnumerable<Edge> source,
			NodeGraph graph,
			EdgeFilter filter)
		{
			return source.Where(e => filter.Include(e));
		}

		public static void RemoveRandom<T>(
			this IList<T> list,
			Chaos chaos,
			int count)
		{
			for (var i = 0; i < count; i++)
			{
				var item = list.Choose(chaos);

				list.Remove(item);
			}
		}
	}
}
