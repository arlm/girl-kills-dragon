#region Namespaces

using System;
using System.Collections;
using System.Collections.Generic;
using GirlKillsDragon.Details;
using GirlKillsDragon.Plots;

#endregion

namespace GirlKillsDragon.Locations
{
	/// <summary>
	/// A manager class that handles creating and coordinating various locations within the story.
	/// </summary>
	public class LocationManager : IEnumerable<Location>, IRegisteredManager
	{
		private readonly Chaos _chaos;

		private readonly List<Location> _locations = new List<Location>();

		private readonly NounFactory _nounFactory;

		private readonly Dictionary<Location, Dictionary<Location, Route>> _routing
			= new Dictionary<Location, Dictionary<Location, Route>>();

		private int _nextLocationId;

		public LocationManager(
			Chaos chaos,
			NounFactory nounFactory)
		{
			// Save the variables we need.
			_chaos = chaos;
			_nounFactory = nounFactory;
		}

		public int Count => _locations.Count;

		public List<Route> Routes { get; } = new List<Route>();

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<Location> GetEnumerator()
		{
			return _locations.GetEnumerator();
		}

		/// <summary>
		/// Constructs a new location for management.
		/// </summary>
		/// <returns></returns>
		public Location Create(string selector = "environment")
		{
			var location = new Location(_nextLocationId++)
			{
				Noun = _nounFactory.Create(selector)
			};

			_locations.Add(location);

			return location;
		}

		public void EstablishDirectRoute(Location location1, Location location2)
		{
			// Figure out the distance between these two points.
			var km = _chaos.NextDouble() * 300 + 1;
			var m = 1000 * km;
			var route = new Route(location1, location2, m);

			location2.Distance = location1.Distance + route.Meters;

			// Create the route in memory so we can easily map directions. This is bi-directional to simplify
			// the lookup.
			if (!_routing.ContainsKey(location1))
			{
				_routing[location1] = new Dictionary<Location, Route>();
			}

			if (!_routing.ContainsKey(location2))
			{
				_routing[location2] = new Dictionary<Location, Route>();
			}

			_routing[location1][location2] = route;
			_routing[location2][location1] = route;
			Routes.Add(route);
		}

		private double? GetShortest(Location start, Location end, HashSet<Location> seen)
		{
			// If we've already seen it, then we can't find a path.
			if (seen.Contains(start))
			{
				return null;
			}

			// We have to update our seen list to avoid infinite loops. So we clone it (so we reset
			// for each one) and then add our current node into it.
			var newSeen = new HashSet<Location>(seen) {start};

			// See if we have a direct connection between these two.
			if (_routing[start].ContainsKey(end))
			{
				return _routing[start][end].Meters;
			}

			// Otherwise, we have to loop through each path and find it.
			double? meters = null;

			foreach (var next in _routing[start].Keys)
			{
				// Don't want to keep recursing into ourselves or our destination, we know those aren't it.
				// We also don't want to recurse into seen ones. This will reduce our looping.
				if (next == start || next == end || seen.Contains(next))
				{
					continue;
				}

				// Look for the shortest path.
				var nextMeters = GetShortest(next, end, newSeen);

				if (nextMeters.HasValue && (!meters.HasValue || nextMeters.Value < meters.Value))
				{
					meters = nextMeters;
				}
			}

			// Return the resulting meters.
			return meters;
		}

		private TimeSpan GetTimeSpan(Location start, Location end)
		{
			// See if we have a direct connection.
			var meters = GetShortest(start, end, new HashSet<Location>());

			if (!meters.HasValue)
			{
				throw new Exception("Cannot find a route.");
			}

			// The average person walks 5.0 km / h. Just pretend we are in Tolkein's world.
			return TimeSpan.FromHours(meters.Value / 1000 / 5);
		}

		/// <summary>
		/// Create a relative time span that represents the time to travel between any two locations.
		/// </summary>
		public PlotTimeSpan GetTravelTime(Location start, Location end)
		{
			return new PlotTimeSpan(() => GetTimeSpan(start, end));
		}

		/// <summary>
		/// Removes all locations that are not connected to another location.
		/// </summary>
		public void RemoveUnconnected()
		{
			_locations.RemoveAll(l => !l.Distance.HasValue);
		}
	}
}
