#region Namespaces

using GirlKillsDragon.Details;

#endregion

namespace GirlKillsDragon.Locations
{
	public class Location
	{
		public Location(int locationId)
		{
			LocationId = locationId;
		}

		public Country Country { get; set; }

		/// <summary>
		/// The distance from the initial location in meters.
		/// </summary>
		public double? Distance { get; set; }

		public int LocationId { get; }

		public string Name => Noun.GetValue("name");

		public Noun Noun { get; set; }

		public string Terrain => Noun.GetValue("terrain");

		public override string ToString()
		{
			return $"L{LocationId}";
		}
	}
}
