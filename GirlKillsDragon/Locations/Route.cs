namespace GirlKillsDragon.Locations
{
	public class Route
	{
		public Route(Location location1, Location location2, double meters)
		{
			Location1 = location1;
			Location2 = location2;
			Meters = meters;
		}

		public Location Location1 { get; set; }

		public Location Location2 { get; set; }

		/// <summary>
		/// The distance of the route in meters.
		/// </summary>
		public double Meters { get; set; }
	}
}
