#region Namespaces

using Autofac;
using GirlKillsDragon.Details;
using GirlKillsDragon.IO;
using GirlKillsDragon.Nodes;
using GirlKillsDragon.Plots;
using GirlKillsDragon.Plots.Injectors;
using GirlKillsDragon.Plots.Resolvers;
using GirlKillsDragon.Plots.Seeds;
using GirlKillsDragon.Plots.Validators;
using GirlKillsDragon.Writing;

#endregion

namespace GirlKillsDragon
{
	/// <summary>
	/// Registers the various classes that can be wired up by the inversion of control.
	/// </summary>
	public class CoreModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			// Most of the core logic is in de-facto singletons.
			builder.RegisterType<Chaos>().AsSelf().SingleInstance();
			builder.RegisterType<Author>().AsSelf().SingleInstance();
			builder.RegisterType<Project>().AsSelf().SingleInstance();
			builder.RegisterType<NodeGraph>().AsSelf().SingleInstance();

			// Register the managers.
			builder.RegisterAssemblyTypes(GetType().Assembly)
				.AssignableTo<IRegisteredManager>()
				.AsSelf()
				.AsImplementedInterfaces()
				.SingleInstance();

			// Nouns and Details
			builder.RegisterType<NounTemplateManager>().AsSelf().SingleInstance();
			builder.RegisterType<NounSelectorManager>().AsSelf().SingleInstance();
			builder.RegisterType<DetailSetManager>().AsSelf().SingleInstance();
			builder.RegisterType<NounFactory>().AsSelf().SingleInstance();

			// Register IO methods.
			builder.RegisterType<DgmlProjectWriter>().AsSelf();
			builder.RegisterType<DotProjectWriter>().AsSelf();
			builder.RegisterType<JsonProjectWriter>().AsSelf();
			builder.RegisterType<DetailsLoader>().AsSelf();

			// Register all the resolvers in a single shot. We register both as a type to figure out
			// resolver ordering and also an interface for the generate command.
			builder.RegisterAssemblyTypes(GetType().Assembly)
				.AssignableTo<IResolver>()
				.AsSelf()
				.AsImplementedInterfaces()
				.SingleInstance();

			// Most of the verification logic is to detect any bugs in our code.
			builder.RegisterType<RemoveOrphanNodesResolver>().AsSelf().SingleInstance();
			builder.RegisterType<PlotInjection>().AsSelf().SingleInstance();
			builder.RegisterType<PlotArranger>().AsSelf().SingleInstance();

			// Register all the factories that insert plots into the tree. We
			// use property injection with these to avoid boilerplate overhead
			// of defining a do-nothing constructor for every single one.
			builder.RegisterAssemblyTypes(GetType().Assembly)
				.AssignableTo<PlotSeedBase>()
				.As<PlotSeedBase>()
				.PropertiesAutowired();
			builder.RegisterAssemblyTypes(GetType().Assembly)
				.AssignableTo<PlotInjectorBase>()
				.As<PlotInjectorBase>()
				.PropertiesAutowired();

			// Load all the validators into memory.
			builder.RegisterAssemblyTypes(GetType().Assembly)
				.AssignableTo<IValidator>()
				.AsImplementedInterfaces();

			// Set up the sentence builder.
			builder.RegisterType<WriterContext>().AsSelf();
		}
	}
}
