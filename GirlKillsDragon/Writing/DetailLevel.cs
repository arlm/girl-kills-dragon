namespace GirlKillsDragon.Writing
{
	public enum DetailLevel
	{
		None,

		Reminder,

		Medium,

		Detailed
	}
}
