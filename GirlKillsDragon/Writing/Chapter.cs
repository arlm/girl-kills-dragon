#region Namespaces

using System.Collections.Generic;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using GirlKillsDragon.Plots;

#endregion

namespace GirlKillsDragon.Writing
{
	/// <summary>
	/// A logical unit in the narrative that groups a single POV and location
	/// into a single group.
	/// </summary>
	public class Chapter : List<Node>
	{
		public Chapter(int number, Actor actor, Location location, PlotDateTime dateTime)
		{
			Number = number;
			Actor = actor;
			Location = location;
			DateTime = dateTime;
		}

		/// <summary>
		/// The most significant actor in the chapter, also known as the point of view.
		/// </summary>
		public Actor Actor { get; set; }

		public bool ActorChanged { get; set; }

		public PlotDateTime DateTime { get; set; }

		public Location Location { get; }

		public bool LocationChanged { get; set; }

		public int Number { get; }

		public bool TimePassed { get; set; }

		public new void Add(Node node)
		{
			base.Add(node);
			node.Chapter = this;
		}
	}
}
