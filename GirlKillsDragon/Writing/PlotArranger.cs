#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using GirlKillsDragon.Plots;
using Serilog;

#endregion

namespace GirlKillsDragon.Writing
{
	/// <summary>
	/// Primary class for arranging various plot items into a series of
	/// chapters.
	/// </summary>
	public class PlotArranger
	{
		private readonly NodeGraph _graph;

		private readonly ILogger _log;

		private readonly NodeManager _nodes;

		public PlotArranger(
			ILogger log,
			NodeManager nodes,
			NodeGraph graph)
		{
			_log = log;
			_nodes = nodes;
			_graph = graph;
		}

		public TimeSpan TimePassedThreshold => TimeSpan.FromHours(1);

		/// <summary>
		/// Arranges the plots into chapters.
		/// </summary>
		public List<Chapter> Arrange()
		{
			// In general, we want to keep chapters together to avoid breaking in the middle of action to
			// show 1-2 nodes of another POV before going back. To do this, we need to keep track of all
			// the plots we have already added. If we see that, we know not to add them to the current chapter.
			var arrangedNodes = new HashSet<Node>();

			// Get an ordered list of all plots in date-time order.
			var plots = _nodes
				.Where(p => p.NodeType == NodeType.Narrative || p.NodeType == NodeType.Epilogue)
				.OrderBy(p => p.DateTime.Resolved)
				.ToList();

			// The initial event is always the first chapter of the novel.
			var chapters = new List<Chapter>
			{
				CreateChapter(plots, 1, arrangedNodes, _nodes.InitialNode)
			};

			// All remaining plots follow the same rules, starting with the latest that we haven't seen.
			var chapterNumber = 1;

			foreach (var plot in plots)
			{
				// If we've already seen this plot, skip it.
				if (arrangedNodes.Contains(plot))
				{
					continue;
				}

				// Add the plot to the chapter.
				var chapter = CreateChapter(plots, ++chapterNumber, arrangedNodes, plot);

				chapters.Add(chapter);
			}

			// Go through and figure out the changed flags.
			Location lastLocation = null;
			Actor lastActor = null;
			PlotDateTime lastDateTime = null;

			foreach (var chapter in chapters)
			{
				// Update the changed flags.
				chapter.LocationChanged = lastLocation != chapter.Location;
				chapter.ActorChanged = lastActor != chapter.Actor;
				chapter.TimePassed = lastDateTime == null
					|| lastDateTime.Resolved + TimePassedThreshold < chapter.DateTime.Resolved;

				// Change the pointers.
				lastLocation = chapter.Location;
				lastActor = chapter.Actor;
				lastDateTime = chapter.DateTime;
			}

			// Return the resulting chapters.
			return chapters;
		}

		private Chapter CreateChapter(
			List<Node> nodes,
			int chapterNumber,
			HashSet<Node> arrangedNodes,
			Node node)
		{
			// Starting with this node, we loop through and follow through with the POV until that breaks.
			var locationActors = new ActorCollection(
				_graph.GetPresentActors(node)
					.Union(node.InActors)
					.Distinct());
			var actor = locationActors.MostSignificantActor;

			_log.Verbose(
				"Chapter {0}: Present Actors {1}",
				chapterNumber,
				_graph.GetPresentActors(node).Names);
			_log.Verbose(
				"Chapter {0}: {2} Actors {1}",
				chapterNumber,
				node.InActorNames,
				node);
			_log.Verbose(
				"Chapter {0}: {1} is primary of {2}",
				chapterNumber,
				actor,
				locationActors.Names);

			// Create a new chapter with this first node.
			var chapter = new Chapter(chapterNumber, actor, node.Location, node.DateTime) {node};

			arrangedNodes.Add(node);

			// Get a subset of all plots that happen in the same location after this first node.
			var nodeIndex = nodes.IndexOf(node);
			var followNodes = nodes
				.Skip(nodeIndex + 1)
				.Where(p => p.Location == node.Location)
				.ToList();

			// Now, go through each of the exit links for this specific actor and add that.
			var dateTime = node.DateTime.Resolved;

			foreach (var followNode in followNodes)
			{
				// See if too much time has passed. If it has, then we skip this node.
				if (dateTime + TimePassedThreshold < followNode.DateTime.Resolved)
				{
					break;
				}

				// Add this node to the list.
				chapter.Add(followNode);
				arrangedNodes.Add(followNode);

				// Update the date time so we check the last one in the chapter.
				dateTime = followNode.DateTime.Resolved;

				// If this chapter is a leave node and the POV is leaving, we're done.
				if (followNode is LeaveNode leaveNode && leaveNode.InActors.Contains(actor))
				{
					break;
				}
			}

			// Return the resulting chapter.
			return chapter;
		}
	}
}
